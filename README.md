# avaj-launcher - @WeThinkCode_

This project is the introduction to the Java world at WeThinkCode_. This is an implementation of an aircraft simulation program based on the class UML diagram.

## Description
Aim of this project is to implement an aircraft simulation program based on the UML class diagram. All classes are required to be implemented respecting every detail provided in the diagram. If necessary, add more classes or include additional attributes, but changing access modifiers and class hireharchy for the classes provided in the diagram are forbidden.

## UML Diagram
![diagram](avaj_uml.jpg "diagram")

## Compile and run
```
git clone this project
cd avaj-launcher/src/
````
Before launching make sure that you have Java installed and `java` & `javac` commands are available in your terminal. If not, click [here](https://java.com/en/download/help/mac_install.xml) to install Java for Mac.

In the root of project folder run the commands given bellow:
```
find . -name '*.java' > sources.txt
javac -sourcepath . @sources.txt
java -classpath ./src com.mndlovu.simulator.Simulator $1
````
Or you can use run.sh in the root of project folder. To launch use:
```
sh run.sh
```
